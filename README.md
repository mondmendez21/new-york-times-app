## New York Times

This Project is for the completion of the requirements needed for Frontend Developer application in 7 Peaks Software.
This program implemented New York Times API for backend and Angular Framework for frontend.


## Cloning the project

1. Create a new folder
2. Inside the created folder, right click > git bash. Or use cmd and cd to the created folder directory.
3. From git bash or cmd, type git clone https://bitbucket.org/mondmendez21/new-york-times-app.git
4. Open VS Code and open the created_folder/`new-york-times-app`. Must be inside the new-york-times-app before starting the app.


## Instructions for starting the app

1. Run `npm install --save @angular-devkit/build-angular` from terminal.
2. Run `ng serve`.
3. Navigate to `http://localhost:4200/`.