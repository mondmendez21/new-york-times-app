export interface DataTableParameters {
    limit: number;
    offset: number;
    sort: string;
    search: string;
    page: number;
}