import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';

import { Observable, of, timer } from 'rxjs';
import { tap, switchMap, delay, debounce, debounceTime } from 'rxjs/operators';

@Injectable()
export class HttpTimerInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(debounce(() => timer(100)));
    }
}