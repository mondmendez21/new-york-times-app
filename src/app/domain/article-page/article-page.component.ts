import { Component, OnInit, Input } from '@angular/core';
import { ArticleDTO } from 'src/app/dto/article-dto';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/data/api.service';
import { ResponseDTO } from 'src/app/dto/response-dto';
import { DataDTO } from 'src/app/dto/data-dto';
import { MetaDTO } from 'src/app/dto/meta-dto';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { LoadingScreenService } from "src/app/services/data/loading-screen.service";
import { Subscription } from "rxjs";

@Component({
  selector: 'app-article-page',
  templateUrl: './article-page.component.html',
  styleUrls: ['./article-page.component.css']
})
export class ArticlePageComponent implements OnInit {

  constructor(
    private route: ActivatedRoute, 
    private apiService: ApiService, 
    private location: Location, 
    private router: Router,
    private loadingScreenService: LoadingScreenService) { }
  
  articles: ArticleDTO[];
  dateToday: Date;

  articleId: string;
  search: string;
  page: number;
  sort: string;
  source: string;
  section: string;
  url: string;
  publishedDateStr: string;
  publishedDate: Date;
  loading: boolean = false;
  loadingSubscription: Subscription;

  meta: MetaDTO = {
    hits: 0,
    offset: 0,
    time: 0,
  }

  data: DataDTO = {
    docs: [],
    meta: this.meta
  }

  responseBody: ResponseDTO = {
    status: '',
    copyright: '',
    response: this.data
  };

  article: ArticleDTO = {
    lead_paragraph: '',
    abstract: '',
    snippet: '',
    headline: {},
    pub_date: '',
    web_url: '',
    _id: '',
    byline: {},
    section_name: '',
    publishedDate: new Date()
  }

  ngOnInit(): void {
    this.loadingSubscription = this.loadingScreenService.loadingStatus.subscribe((value) => {
      this.loading = value;
    });

    this.dateToday = new Date();

    this.route.queryParams
    .subscribe(params => {

      this.articleId = params.webId;
      this.search = params.search;
      this.page = params.page;
      this.sort = params.sort;
      this.section = params.section;
      this.source = params.source;
      this.publishedDateStr = params.publishedDate;
      this.url = params.url;
    })

    this.publishedDate = new Date(this.publishedDateStr);

    this.searchArticle(this.url)
  }

  /** Function that handles the RETURN TO HOMEPAGE button. */
  goBack(): void {
    this.router.navigate(['/home'], { queryParams : { search : this.search, page : this.page, sort : this.sort } } );
  }

  /** Searches article by its HTML link. */
  searchArticle(webUrl: string) {
    const proxyurl = "https://cors-anywhere.herokuapp.com/"; //used proxy as a workaround in handling CORS
    var url = webUrl;
    fetch(proxyurl + url)
    .then(response => response.text())
    .then(function (html) {
      
      var parser = new DOMParser();
      var result = parser.parseFromString(html, 'text/html');

      document.getElementById("title-string").innerHTML = result.getElementsByClassName("css-1vkm6nb")[0].innerHTML

      for (let i=0; i < result.getElementById("story").getElementsByClassName("StoryBodyCompanionColumn").length; i ++) {
        document.getElementById("article-string").innerHTML += result.getElementById("story").getElementsByClassName("StoryBodyCompanionColumn")[i].innerHTML;
      }
    })
    .catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"));
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }


}
