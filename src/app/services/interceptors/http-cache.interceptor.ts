import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';

import { HttpCacheService } from '../data/http-cache.service';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class HttpCacheInterceptor implements HttpInterceptor {
    constructor (private httpCacheService: HttpCacheService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.method !== 'GET') {
            this.httpCacheService.clearCache();
            return next.handle(req);
        }

        const cachedResponse: HttpResponse<any> = this.httpCacheService.get(req.url);

        if (cachedResponse) {
            return of(cachedResponse);
        }

        return next.handle(req)
            .pipe (tap(event => {
                if (event instanceof HttpResponse) {
                    this.httpCacheService.set(req.url, event);
                }
            })
        );
    }
}
