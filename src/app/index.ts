import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpCacheInterceptor } from './services/interceptors/http-cache.interceptor';
import { HttpTimerInterceptor } from './services/interceptors/http-timer-interceptor';
import { LoadingScreenInterceptor } from './services/interceptors/http-loading.interceptor';
import { Router } from '@angular/router';

export const httpInterceptorProviders = [
    {
        provide: HTTP_INTERCEPTORS,
        useClass: HttpCacheInterceptor,
        multi: true
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: HttpTimerInterceptor,
        multi: true
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: LoadingScreenInterceptor,
        multi: true
    }
];