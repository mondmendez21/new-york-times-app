import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root'})
export class HttpCacheService {
    private request: any = {};

    set(endpoints: string, response: HttpResponse<any>): void {
        this.request[endpoints] = response;
    }

    get(endpoints: string): HttpResponse<any> | undefined | null {
        return this.request[endpoints];
    }

    clearCache(): void {
        this.request = {};
    }

    clearEndpointsCache(endpoints: string): void {
        this.request[endpoints] = undefined;
    }
}
