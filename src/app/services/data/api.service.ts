import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ResponseDTO } from '../../dto/response-dto';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { ArticleDTO } from '../../dto/article-dto';
import { DataDTO } from '../../dto/data-dto'
import { DataTableParameters } from '../../dto/datatable-parameters';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiURL: string = 'https://api.nytimes.com/svc/';
  apiKey: string = 'gibHOJgAPhuUr9Xv1sXHQsGYxcGwsWkF';
  fl: string = 'lead_paragraph,abstract,snippet,headline,pub_date,web_url,_id,byline,section_name'

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private httpClient: HttpClient) { }

  searchArticles(parameters: DataTableParameters): Observable<ResponseDTO> {

    var object = this.httpClient.get<ResponseDTO>(`${this.apiURL}search/v2/articlesearch.json?q=${parameters.search}&sort=${parameters.sort}&fl=${this.fl}&fq=document_type:("article")&page=${parameters.page}&api-key=${this.apiKey}`, this.httpOptions)
    return object
    .pipe(
      tap(x => x.response.docs.length),

      catchError(this.handleError<ResponseDTO>('searchArticles', ))
    );
  }

  searchArticleById(id: string): Observable<ResponseDTO> {

    var object = this.httpClient.get<ResponseDTO>(`${this.apiURL}search/v2/articlesearch.json?fq=_id:("${id}")&api-key=${this.apiKey}`, this.httpOptions)
    return object
    .pipe(
      tap(x => x.response.docs.length) ,

      catchError(this.handleError<ResponseDTO>('searchArticleById', ))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
