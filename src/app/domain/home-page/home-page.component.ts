import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables/src/angular-datatables.directive';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { ResponseDTO } from 'src/app/dto/response-dto';
import { ApiService } from 'src/app/services/data/api.service';
import { ArticleDTO } from 'src/app/dto/article-dto';
import { DataTableParameters } from 'src/app/dto/datatable-parameters';
import { DataDTO } from 'src/app/dto/data-dto';
import { MetaDTO } from 'src/app/dto/meta-dto';
import { Observable, fromEvent } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoadingScreenService } from "src/app/services/data/loading-screen.service";
import { Subscription } from "rxjs";
import {
  debounceTime, distinctUntilChanged, switchMap, tap
} from 'rxjs/operators';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  parameters: DataTableParameters = {
    limit: 0,
    offset: 0,
    sort: '',
    search: '',
    page: 0
  };

  meta: MetaDTO = {
    hits: 0,
    offset: 0,
    time: 0,
  }

  data: DataDTO = {
    docs: [],
    meta: this.meta
  }

  responseBody: ResponseDTO = {
    status: '',
    copyright: '',
    response: this.data
  };

  article: ArticleDTO = {
    lead_paragraph: '',
    abstract: '',
    snippet: '',
    headline: {},
    pub_date: '',
    web_url: '',
    _id: '',
    byline: {},
    section_name: '',
    publishedDate: new Date()
  }

  searchString: string;

  response$: Observable<ResponseDTO>;
  articles$: Observable<ArticleDTO[]>;
  dateToday: Date;
  publishedDateStr: string;
  publishedDate: Date;

  doc: any = {}
  searchDebouncer$: Subject<string> = new Subject();

  searchTerms = new Subject<string>();
  inputValue: string = '';
  sortValue: string = '';

  debouncedInputValue = this.inputValue;
  article$: Subject<any> = new Subject();
  config: any;

  maxSize: number = 10;
  autoHide: boolean = false;
  responsive: boolean = true;
  labels: any = {
      screenReaderPaginationLabel: 'Pagination',
      screenReaderPageLabel: 'page',
      screenReaderCurrentLabel: `You're on page`
  };

  articles: ArticleDTO[];
  loading: boolean = false;
  loadingSubscription: Subscription;

  constructor(
    private apiService: ApiService, 
    private router: Router, 
    private route: ActivatedRoute, 
    private http: HttpClient, 
    private loadingScreenService: LoadingScreenService) { 
    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: 0
    };
  }

  ngOnInit(): void {
    this.loadingSubscription = this.loadingScreenService.loadingStatus.subscribe((value) => {
      this.loading = value;
    });

    this.dateToday = new Date();
    this.parameters.sort = 'newest';
    this.parameters.search = '';

    this.route.queryParams
      .subscribe(params => {
        if (params.search == undefined) {
          this.inputValue = '';
        } else {
          this.inputValue = params.search;
          this.parameters.search = params.search;
        }

        if (params.sort == undefined) {
          this.sortValue = 'Newest';
        } else {
          this.sortValue = params.sort;
          this.parameters.sort = params.sort;
        }

        if (params.page == undefined) {
          this.config.currentPage = 1;
        } else {
          this.config.currentPage = params.page;
          this.parameters.page = this.config.currentPage - 1;
        }
      });

    this.setupSearchDebouncer();
    
    this.apiService.searchArticles(this.parameters)
      .subscribe(
        data => {
          this.responseBody = data as ResponseDTO;
          this.articles = this.responseBody.response.docs;
          this.articles.forEach(article => {
            article.publishedDate = new Date(article.pub_date);
          })
          if (this.responseBody.response.meta.hits >= 2000) {
            this.config.totalItems = 2000;
          } else {
            this.config.totalItems = this.responseBody.response.meta.hits;
          }
        }
      )

  }

  /** Function that handles any changes made at the search input. */
  onSearchInputChange(term: string): void {
    this.searchDebouncer$.next(term);
  }

  /** Function that controls massive API calls everytime you search for a word. */
  setupSearchDebouncer(): void {

    this.searchDebouncer$.pipe(
      debounceTime(1500),
      distinctUntilChanged(),
    ).subscribe((searchTerm: string) => {

      this.debouncedInputValue = searchTerm;
      this.searchArticles(searchTerm);
    });
  }

  /** Function that handles the search for the articles. */
  searchArticles(searchTerm: string): void {

    this.article$.next(null);
    this.parameters.search = searchTerm;
    this.parameters.page = 0;
    this.config.currentPage = 1;

    this.apiService.searchArticles(this.parameters)
      .subscribe(
        data => {
          this.responseBody = data as ResponseDTO;
          this.articles = this.responseBody.response.docs;

          this.articles.forEach(article => {
            article.publishedDate = new Date(article.pub_date);
          })
        }
      )
  }

  /** Function that handles any page changes. An API call at New York Times returns 10 articles. */
  pageChanged(event){
    this.config.currentPage = event;
    this.parameters.page = this.config.currentPage - 1;
    this.parameters.search = this.inputValue;

    this.apiService.searchArticles(this.parameters)
      .subscribe(
        data => {
          this.responseBody = data as ResponseDTO;
          this.articles = this.responseBody.response.docs;

          this.articles.forEach(article => {
            article.publishedDate = new Date(article.pub_date);
          })
        }
      )
  }

  /** Function that handles the viewing of article. It will redirect you to the article detail page. */
  viewArticle(id: string): void {
    var article = this.articles.find(article => article._id == id);
    this.router.navigate(['/article'], 
    { queryParams: 
      { search: this.inputValue, 
        page: this.config.currentPage,
        sort: this.parameters.sort, 
        section: article.section_name,
        source: article.byline.original,
        publishedDate: article.pub_date,
        webId: id,
        url: article.web_url
      } 
    });
  }

  /** Function that handles sorting of the articles by newest or oldest. */
  onClickSortEvent(): void {
    if (this.sortValue.toLowerCase() == 'newest') {
      this.sortValue = 'Oldest';
      this.parameters.sort = 'oldest'
    } else if (this.sortValue.toLowerCase() == 'oldest') {
      this.sortValue = 'Newest';
      this.parameters.sort = 'newest'
    }

    this.apiService.searchArticles(this.parameters)
      .subscribe(
        data => {
          this.responseBody = data as ResponseDTO;
          this.articles = this.responseBody.response.docs;
        }
      )
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }
}
