export class MetaDTO {
    hits: number;
    offset: number;
    time: number;
}