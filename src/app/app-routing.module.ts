import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticlePageComponent } from './domain/article-page/article-page.component'
import { HomePageComponent } from './domain/home-page/home-page.component'
import { ArticleDTO } from './dto/article-dto'

const routes: Routes = [
  { path: 'article', component: ArticlePageComponent },
  { path: 'home', component: HomePageComponent },
  { path: '**', redirectTo: 'home', pathMatch: 'full' },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, 
      {
        scrollPositionRestoration: 'enabled'
      }
    ),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
