import { Component } from '@angular/core';
import { ApiService } from './services/data/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'New York Times';

  constructor(private apiService: ApiService){}

  ngOnInit() {

  }
}
