import { DataDTO } from './data-dto';

export class ResponseDTO {
    status: string;
    copyright: string;
    response: DataDTO;
}