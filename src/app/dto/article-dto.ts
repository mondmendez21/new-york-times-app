export class ArticleDTO {
    lead_paragraph: string;
    abstract: string;
    snippet: string;
    headline: any;
    pub_date: string;
    web_url: string;
    _id: string;
    section_name: string;
    byline: any;
    publishedDate: Date;
}