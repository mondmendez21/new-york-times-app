import { ArticleDTO } from './article-dto';
import { MetaDTO } from './meta-dto';

export class DataDTO {
    docs: ArticleDTO[];
    meta: MetaDTO;
}