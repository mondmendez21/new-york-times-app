import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { HomePageComponent } from './domain/home-page/home-page.component';
import { DataTablesModule } from 'angular-datatables';
import { HeaderComponent } from './domain/header/header.component';
import { HomeComponent } from './home/home.component';
import { ArticlePageComponent } from './domain/article-page/article-page.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { httpInterceptorProviders } from './';
import { FooterComponent } from './domain/footer/footer.component';
import { LoadingScreenComponent } from './loading-screen/loading-screen.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    HeaderComponent,
    HomeComponent,
    ArticlePageComponent,
    FooterComponent,
    LoadingScreenComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    DataTablesModule,
    NgxPaginationModule,
  ],
  providers: [
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
